#! /bin/bash

module add python

topdir=`pwd`
cd ./cExtens
python setup.py clean --all
python setup.py build
if [[ $? == 0 ]]; then
    echo "copying cWarpImage.so to $topdir/saxswaxs ..."
	python setup.py install --install-lib=$topdir/saxswaxs
else
	echo "build failed"
fi
