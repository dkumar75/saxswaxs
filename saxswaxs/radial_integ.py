#! /usr/bin/env python

import os
import numpy as np
import matplotlib.pyplot as plt

def radial_integration (img, qp, qz, outpath, basename):
	# remove nans
    img[np.isnan(img)] = 0
   
	# calculate radius
    npts = 1500
    r = np.sqrt(qz**2+qp**2)
    qval = np.linspace(r.min(), r.max(), npts)
    dr = qval[1]-qval[0]
    ir = (r / dr).astype(np.int)
    

	# integrate
    tbin = np.bincount(ir.ravel(), img.ravel())
    nr = np.bincount(ir.ravel())
    with np.errstate(divide='ignore', invalid='ignore'):
        radialprofile = tbin / nr
    
    # save textfile
    filename = os.path.join(outpath, basename+'.txt')
    np.savetxt(filename, (qval, radialprofile), fmt='%1.5e')
