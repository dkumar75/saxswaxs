#! /usr/bin/env python

import numpy as np
from numpy import union1d as union
from scipy.ndimage import gaussian_filter
from scipy.ndimage.morphology import  binary_dilation, binary_erosion

def balanced_mean (img):
    qmean = img.mean ()
    err = np.Inf
    while err > 1.0E-06:
        m1 = img[ img < qmean].mean()
        m2 = img[ img > qmean].mean()
        qprev = qmean
        qmean = 0.5 * m1 + 0.5 * m2
        err = np.abs(1-qmean/qprev)
    return qmean

def create_mask (img, cen):
    ny, nx = img.shape

    # create mask for modules in horizontal dir
    i1 = list()
    for i in range(ny):
        if np.all (img[i,:] == 0):
            i1.append (i)

    # extend 2 pixels on each side
    i1 = np.array (i1, dtype=int)
    ii = union(i1, union(i1-1, union(i1-2, union(i1+1, i1+2))))
    
    # create mask for modules in vertical direction
    j1 = list()
    for j in range(nx):
        if np.all (img[:,j] == 0):
            j1.append (j)

    # extend 2 pixels on each side
    j1 = np.array (j1, dtype=int)
    jj = union(j1, union(j1-1, union(j1-2, union(j1+1, j1+2))))

    mask1 = np.zeros ((ny, nx), dtype=bool)
    mask1[ii,:] = True
    mask1[:,jj] = True

    ### search beamstop ###

    # filter out some of the noise    
    img1 = gaussian_filter (img, sigma=2)

    # beamstop has low intensity (but not zero)
    # remove all the high intensity stuff
    qcut = balanced_mean (img1)
    mask2 = img < qcut

    # remove module gaps 
    mask2[ mask1 ] = False

     #this should (hopefully) saturate the beamstop
    for i in range (5):
        mask2 = binary_erosion (mask2)
        mask2 = binary_dilation (mask2, iterations=2)

    # return value
    return np.logical_not (np.logical_or (mask1, mask2))
