#! /usr/bin/env python

import numpy as np
import scipy
from scipy import signal


def findpeaks(Y):

    # Find peaks using continuous wavelet transform; parameter is the range of peak widths (may need tuning)
    peakindices=scipy.signal.find_peaks_cwt(Y,np.arange(5,30))
    return peakindices

def sdd_approx (img, agb):
    """
    Approximate the sample to detector distance by finding the lowest q peak and matching with the AgB standard
    """

    # Get the image center
    cen = agb.center_

    # Find radially averaged intensities
    y, x = np.indices((img.shape))
    r = np.sqrt((x - cen[1])**2 + (y - cen[0])**2)
    r = r.astype(np.int)
    tbin = np.bincount(r.ravel(), img.ravel())
    nr = np.bincount(r.ravel())
    with np.errstate(divide='ignore', invalid='ignore'):
        radialprofile = tbin / nr

    # Find peak positions, they represent the radii
    #with np.errstate(invalid='ignore'):
    #    peaks=findpeaks(np.nan_to_num(np.log(radialprofile+1)))

    # Get the tallest peak
    bestpeak = radialprofile.argmax()

    # Calculate sample to detector distance for lowest q peak and return
    sdd = bestpeak * agb.pixel_size_[0] / agb.tantth_[0]
    return sdd
