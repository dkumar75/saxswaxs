#! /usr/bin/env python

from __future__ import print_function
import fabio
import pickle
import os
import re
import glob
import numpy as np
from agb_data import AgBData

def load_logimage(edfpath, ext):
    if ext == "edf":
        data = fabio.open(edfpath).data
    elif ext == "gb":
        data = (np.fromfile(edfpath, dtype=np.float32)).reshape(1679, 1475)
    else:
        raise IOError
    data[np.isnan(data)] = 1
    data[data < 1] = 1
    img = np.log(data)
    return img


def load_image(edfpath, ext):
    if re.search("edf$", edfpath):
        return fabio.open(edfpath).data
    elif re.search("gb$", edfpath):
        return (np.fromfile(edfpath, dtype=np.float32)).reshape(1679, 1475)
    else:
        raise IOError

def get_incedent_angle(edfpath):
    # first try tiled image
    res = re.search('_sfloat_', edfpath)
    if res is not None:
        txtfile = edfpath[:res.start()]+'_lo_'+edfpath[res.end():-2]+'txt'
    else:
	try:
            txtfile = os.path.splitext(edfpath)[0] + '.txt'
            if not os.path.isfile(txtfile):
                raise IOError
        except IOError as e:
            # it may be the burst mode
            basename = os.path.splitext(edfpath)[0]
            obj = re.search('_\d+$', basename)
            if obj is not None:
                iend = obj.start()
                token = basename[:iend] + '*txt'
                txtfile = glob.glob(token)[0]
            else:
                return None
    fp = open(txtfile, 'r')
    lines = fp.readlines()
    for line in lines:
        if re.match ('Sample', line) and re.search ('Alpha', line):
            a = re.search('\d+\.\d+',line).group()
            return np.deg2rad(float(a))
        elif re.match('Alpha=', line): # older txt file format
            a = re.search('\d+\.\d+',line).group()
            return np.deg2rad(float(a))
    return None

def save_data(data, edf, a, output, basename):
    try:
        header = fabio.open(edf).header
        header['qpar_min'] = a.qrange_[0]
        header['qpar_max'] = a.qrange_[1]
        header['qvrt_min'] = a.qrange_[2]
        header['qvrt_max'] = a.qrange_[3]

        # create an edf object
        edfobj = fabio.edfimage.edfimage()
        edfobj.setHeader (header)
        edfobj.setData(data)

        # path
        filename = basename + '.edf'
        path = os.path.join(output, filename)
        edfobj.write(path, force_type=np.float32)
    except TypeError as e:
	# save data as bindary dump
	filename = os.path.join(output, basename + '.gb')
	fp = open(filename, 'wb')
	data.tofile(fp)
	fp.close()


def load_calibration(filename):
    fp = open(filename, 'r')
    a = pickle.load(fp)
    fp.close()
    return a

def dump_calibration(filename, data):
    fp = open(filename, 'w')
    pickle.dump(data, fp)
    fp.close()
