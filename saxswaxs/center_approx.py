#! /usr/bin/env python

import sys
import numpy as np
from scipy import signal

def eval_linecuts(cen, img, angle):
    """
    defines a line-cut from center to edge of the image,
    and returns max intensity value and its position.
    """
    tana = np.tan(angle)
    ny,nx = img.shape
    if angle < 0.5 * np.pi:
        jj = np.arange(cen[1],nx)
    else:
        jj = np.arange(cen[1])
    
    ii = (cen[0] - tana * (jj - cen[1])).astype(int)
    ind = np.logical_and(ii >= 0, ii < ny)
    if np.count_nonzero(ind) > 0:
        ii = ii[ind]
        jj = jj[ind]
        y = img[ii,jj]
        if angle > 0.5 * np.pi:
            y = y[::-1]
        return y
    else:
        return None
 
def center_test(cen, img):
    """
    test the center for accuracy. The profile of line-cuts
    are compared against the radial profile, provided center
    is not on/near a gap module, they should have similar highest peak
    and corresponding peak position.
    """
    #calculate data radial profile
    y, x = np.indices(img.shape)
    r = np.sqrt((x - cen[1]) ** 2 + (y - cen[0]) ** 2)
    r = r.astype(np.int)
    tbin = np.bincount(r.ravel(), img.ravel())
    nr = np.bincount(r.ravel())
    with np.errstate(divide='ignore', invalid='ignore'):
            radialprofile = tbin / nr
    peakpos =  radialprofile.argmax()
    vavgmax  = radialprofile[peakpos]

    # if intensity is too low, don't even bother to go any further
    if vavgmax < 1000:
        return False

    ## make 5 line-cuts and get position of max intensity and its position ##
    ## compare them with avg, values                                       ##
    
    # GIWAXS images are more likely to have half-rings, no point going over 180
    tths = np.linspace(0, np.pi, 20)
    count = 0
    for a in tths:
        v = eval_linecuts(cen,img, a)
        if v is None: continue

        # align 1st order of linecut to the radial average
        p = v.argmax()
        if p < peakpos: v = np.pad(v, (peakpos-p,0), 'constant')
        else: v = v[p-peakpos:]
        nmax = min(radialprofile.size, v.size)
        corrcoef = np.corrcoef(radialprofile[:nmax], v[:nmax])[0,1]
        if corrcoef > 0.85:
            count += 1
            if count > 3: return True
    return False

def approx_width(r):
    """
    linearly varies the peak width between GISAXS, where peaks are
    thinner to GIWAXS, where peaks are fewer but wider.
    """
    return (0.047 * r + 1.8261)

def tophat2(radius, scale=1):
    """
    convolution kernel, revolved to form a ring, with Mexican Hat 
    profile along the radial direction. 
    
    radius : peak position along the radius
    scale  : magnification factor
    """
    width = approx_width(radius)
    N = np.round(radius) + 3 * round(width) + 1
    x = np.arange(-N,N)
    x, y = np.meshgrid(x, x)
    t = np.sqrt(x**2 + y**2) - radius
    s = width
    a = scale/np.sqrt(2 * np.pi) / s**3
    w = a * (1 - (t/s)**2) * np.exp(-t**2 / s**2 / 2.)
    return w

def cwt2d(img, radii):
    """
    continuous wavelet transform, works well for GIWAXS images,
    but is rather slow. It is used as backup if other methods fail.
    """
    maxval = 0
    center = np.array([0,0], dtype=np.int)
    for r in radii:
        w = tophat2(r, scale=1000)
        im2 = signal.fftconvolve(img, w, 'same')
        if im2.max() > maxval:
            maxval = im2.max()
            center = np.unravel_index(im2.argmax(), img.shape)
    return center

def auto_correlation_cen(img, log=False):
    """     
    Ron's auto correlation method. works well for most images.
    """
    if log:
        # Rescale brightness of the image with log depth
        img = img.astype(np.float)
        with np.errstate(divide='ignore', invalid='ignore'):
            img = np.log(img * (img > 0) + 1)

    one = np.ones_like(img)
    con = signal.fftconvolve(img, img) / signal.fftconvolve(one, one)
    center = np.array(np.unravel_index(con.argmax(), con.shape)) / 2
    return center
 
def center_approx(img):
    """
    Main center approximation routine.
    """
    cen = auto_correlation_cen(img)
    if not center_test(cen, img):
        # Only GIWAXS images are likely to fail auto-corrlation
        # Test for GIWAXS only
        radii = np.arange(25, 55)
        cen   = cwt2d(img, radii)
        sys.stderr.write("Info: Auto-corrleation failed to find a good center")
    if not center_test(cen, img):
        sys.stderr.write("Fatal error: CWT failed to find a good center approximation.\n")
        sys.stderr.write("Unable to continue data-processing\n")
        exit(1)
    return cen
        
if __name__ == '__main__':
    import re
    import pylab as plt
    import fabio
    import sys
    import os
    import time
    edfs = sys.argv[1:]
    token = re.compile('\d\d\d\d')
    for edf in edfs:
        data = fabio.open(edf).data
        cen = auto_correlation_cen(data)
        if not center_test(cen, data):
            radii = np.arange(25, 55, 1)
            cen = cwt2d(data, radii)
        ax = plt.axes()
        ax.imshow(data)
        ax.plot(cen[0], cen[1], 'ro')
        res = re.search(token, edf)
        if res in None: suffix = '0000'
        else: suffix = res.group()
        figname = 'dummy' +suffix+'.png'
        outf = os.path.join("/tmp", figname)
        plt.savefig(outf)
        del ax
