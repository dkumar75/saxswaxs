#! /usr/bin/env python

from image_load import *
from cWarpImage import warp_image
import numpy as np
import matplotlib.pyplot as plt


def calc_q_range(lims, agb, alphai):
    m2nm = 1.0E+09
    sdd = agb.sdd_ * m2nm
    wavelen = agb.wavelength_ * 0.1 # angstrom -> nanometer
    pixel = agb.pixel_size_ * m2nm

    # calculate q-range for the image
    y = np.array([0, lims[1]-1], dtype=np.float) * pixel[0]
    z = np.array([0, lims[0]-1], dtype=np.float) * pixel[1]
    y,z = np.meshgrid(y,z)
    y -= agb.center_[1] * pixel[0]
    z -= agb.center_[0] * pixel[1]

    # calculate angles
    tmp = np.sqrt(y ** 2 + sdd ** 2)
    cos2theta = sdd / tmp
    sin2theta = y / tmp
    tmp = np.sqrt(z ** 2 + sdd ** 2)
    cosalpha = sdd / tmp
    sinalpha = z / tmp
    k0 = 2. * np.pi / wavelen

    # calculate q-values of each corner
    qx = k0 * (cosalpha * cos2theta - np.cos(alphai))
    qy = k0 * cosalpha * sin2theta
    qz = k0 * (sinalpha + np.sin(alphai))
    qp = np.sign(qy) * np.sqrt(qx ** 2 + qy ** 2)
    q_range = [qp.min(), qp.max(), qz.min(), qz.max()]
    return q_range, k0


def remesh(image, agb, alphai):
    # image size
    shape = image.shape

    # compute q-range
    qrange, k0 = calc_q_range(shape, agb, alphai)

    nz = shape[0]
    dz = (qrange[3] - qrange[2])/(nz-1)
    ny = np.int((qrange[1]-qrange[0])/dz)
    qvrt = np.linspace(qrange[2],qrange[3],nz)
    qpar = qrange[0] + np.arange(ny) * dz
    qpar,qvrt = np.meshgrid(qpar,qvrt)

    m2nm = 1.0E+09
    pixel = agb.pixel_size_ * m2nm
    center = np.zeros(2, dtype=np.float)
    center[0] = agb.center_[0]
    center[1] = agb.center_[1]
    sdd = agb.sdd_ * m2nm
    img = warp_image(image,qpar,qvrt,pixel,center,alphai,k0,sdd,0)
    return img,qpar,qvrt

def remesh_image(filename, agb):
    # load image
    ext = os.path.splitext(filename)[1]
    image = load_image(filename, ext)

    # read incedent angle
    alphai = get_incedent_angle(filename)
    if alphai is None:
        sys.strerr.write('Failed to read indendent angle for %s' % filename)
        exit(1)
    qimg,qp,qz = remesh(image, agb, alphai)
    return qimg,qp,qz
    
def remesh_mask(agbfile, agb):
    alphai = 0
    img = agb.mask_
    qimg,qp,qz = remesh(img, agb, alphai)
    qimg = np.round(qimg)
    return qimg.astype(bool).astype(np.float32)
