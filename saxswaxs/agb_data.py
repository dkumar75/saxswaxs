#! /usr/bin/env python
import sys
import numpy as np

class AgBData:
    def __init__ (self):
        self.hash_ = ''
        self.center_ = np.array([0., 0.], dtype=np.float)
        self.pixel_size_ = np.array([172E-06, 172E-06]) # meters
        self.sdd_ = 0.3 # meters
        self.wavelength_ = 1.23984 # angstrom
        self.tilt_ = 0.0
        self.rot_ = 0.0
        self.qrange_ = [0., 0., 0., 0.]
        dspacings = np.array([58.367, 29.1835, 19.45567, 14.59175], dtype=float) # inv angstrom
        tth = 2 * np.arcsin (self.wavelength_ / 2. / dspacings)
        self.tantth_ = np.tan (tth)
        self.filename_ = None
        self.shape_ = [1, 1]

    def __str__(self):
        s = 'Center: ' + str(self.center_) +'\n'
        s += 'Sample Det. Dist.: {0}\n'.format(self.sdd_)
        s += 'Tilt: {}\n'.format(self.tilt_)
        return s

    def setMask (self, mask):
        self.mask_ = mask

    def setWavelenth (self, wavelen):
        self.wavelength_ = wavelen

    def setTilt (self, tilt):
        self.tilt_ = tilt

    def setRoatation (self, rot):
        self.rot_ = rot

    def setSDD (self, d):
        self.sdd_ = d

    def setCenter (self, cen):
        self.center_ = cen

    def calcQrange(self):
        nrow,ncol = self.shape_
        m2nm = 1.0E+09
        sdd = self.sdd_ * m2nm
        wavelen = self.wavelength_ * 0.1
        pixel = self.pixel_size_ * m2nm
        x = np.array([0, ncol, ncol, 0], dtype=np.float)
        y = np.array([0, 0, nrow, nrow], dtype=np.float) 
        x = (x - self.center_[0]) * pixel[0]
        y = (y - self.center_[1]) * pixel[0]

        # calculate angles
        tmp = np.sqrt(x ** 2 + sdd ** 2)
        sin2theta = x / tmp
        cos2theta = sdd / tmp
        tmp = np.sqrt(y ** 2 + sdd ** 2)
        sinalpha = y / tmp
        cosalpha = sdd / tmp
        k0 = 2. * np.pi / wavelen

        # calculate q-values of each corner
        qx = k0 * (cosalpha * cos2theta - 1)
        qy = k0 * cosalpha * sin2theta
        qz = k0 * sinalpha 
        qp = np.sign(qy) * np.sqrt(qx ** 2 + qy ** 2)
        self.qrange_ = [qp.min(), qp.max(), qz.min(), qz.max()]

    def setQrange (self, qrange):
        if len(qrange) != 4:
            print "input must be a 4-element list"
            return
        for i in range(4):
            self.qrange_[i] = qrange[i]
