#! /usr/bin/env python
from __future__ import division, absolute_import, print_function
import os
import sys
from numpy.distutils.core import Extension
from numpy.distutils.core import setup

ext = Extension(name = 'saxswaxs.cWarpImage',
                 sources = ['cext/cWarpImage.cc', 'cext/remesh.cc' ],
                 extra_compile_args = ['-fopenmp', '-O3', '-ffast-math' ],
                 #extra_compile_args = ['-O0 -g', boost_inc], ### uncomment for debugging
                 extra_link_args =  [ ]
                )

if __name__ == "__main__":
    setup(name = 'saxswaxs',
          description       = "Transform image to new coordinate system, faster",
          version           = "1.0.0",
          author            = "Dinesh Kumar",
          author_email      = "dkumar@lbl.gov",
          packages          = [ 'saxswaxs' ],
          scripts           = [ 'calibrate_data.py', 'test_calibrate_data.sh' ],
          ext_modules       = [ext]
          )
# End of setup_example.py
