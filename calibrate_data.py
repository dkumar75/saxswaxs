#!/usr/bin/env python

from __future__ import division
import os
import glob
import sys
import time
import matplotlib
matplotlib.use('Agg')
import numpy as np
import matplotlib.pyplot as plt
import saxswaxs
import hashlib
import argparse

def calibrate_AgBeh(path, ext):
    if path is None:
        raise Exception("Must provide path to the Silver-Behenate (AgB) file")

    # Create AgB data structer
    calib_agb = saxswaxs.AgBData()
    calib_agb.filename_ = path
    
    try:
        img = saxswaxs.load_image(path, ext)
        calib_agb.shape_ = img.shape
    except IOError as e:
        raise Exception("Unable to open specified Silver-Behenate file")

    # get the approximate center
    cen = saxswaxs.center_approx(img)
    calib_agb.setCenter(cen)

    # approximate SDD
    sdd = saxswaxs.sdd_approx(img, calib_agb)
    calib_agb.setSDD(sdd)

    # Run dpdak saxs_calibration
    calib_agb = saxswaxs.saxs_calibration(img, calib_agb)

    # calculate qrange
    calib_agb.calcQrange()

    # campute mask for the images
    mask = saxswaxs.create_mask(img, cen)
    calib_agb.setMask(mask)

    return calib_agb

def determine_ext(inpath):
    if len(glob.glob(os.path.join(inpath,'*.gb'))) > 0:
        ext = 'gb'
    else:
        ext = 'edf'
    return ext

def calibrate_data(edfiles, agb_data, ext, output=None):
    for edf in edfiles:
        basename = os.path.splitext(edf.split('/')[-1])[0]

        # remesh the data and save image
        try:
            outpath = os.path.join (output, 'png')
            if not os.path.isdir (outpath):
                os.mkdir (outpath)
            qimg,qp,qz = saxswaxs.remesh_image(edf, agb_data)
            qrange = [qp.min(), qp.max(), -1*qz.max(), -1*qz.min()]
            plt.imshow(np.log(qimg+4), extent=qrange)
            plt.savefig(os.path.join(outpath,basename+'.png'))
        except Exception as e:
            print (e)
            print ("Remeshing of %s failed.\n" % edf)

        # write the data as edf file
        try:
            saxswaxs.save_data (qimg, edf, agb_data, output, basename)
        except Exception as e:
            print (e)
            print ("Could not save remeshed data to edf : %s.\n" % edf)

        # radial integration as text file
        try:
            outpath = os.path.join (output, 'integration')
            if not os.path.isdir (outpath):
                os.mkdir (outpath)
            saxswaxs.radial_integration (qimg, qp, qz, outpath, basename)
        except Exception as e:
            print (e)
            print ("Radial integration failed for %s.\n" % edf)

def proeces_args():
    parser = argparse.ArgumentParser(description='Calibrate beamline 7.3.3 data')
    parser.add_argument('inpath', help='path to input data')
    parser.add_argument('outpath', help='path where output will be saved')
    parser.add_argument('-b', '--begin', type=int, help='starting file numbers')
    parser.add_argument('-e', '--end', type=int, help='ending file numbers')
    parser.add_argument('--ext', help='extension of the files to be processed')
    return parser

def main():
    # collect input arguments
    parser = proeces_args()
    args = parser.parse_args()

    # check input path
    inpath = args.inpath
    if not os.path.isdir(inpath):
        sys.stderr.write("Error: invalid path to input files");
        exit(1);

    # check output path
    outpath = args.outpath
    if not os.path.isdir(outpath):
        os.makedirs(outpath)

    ## proces optional arguments ##

    # starting file index
    if args.begin is not None:
        start = args.begin
    else:
        start = 0
    
    # index of last file to be processed
    if args.end is not None:
        end = args.end + 1
    else:
        end = None

    # use this a override
    if args.ext is not None:
        ext = args.ext
    else:
        ext = determine_ext(inpath)
    print "Using extension \""+str(ext)+"\""
  

    # search calibration file
    agbfile = None
    agb_path = os.path.join(inpath, 'calibration')
    agbs = os.listdir(agb_path)
    for f in agbs:
        if f.endswith(ext):
            agbfile = os.path.join(agb_path, f)
            break

    if agbfile is None:
        print ("Error: Calibration file found.\n")

    if not os.path.isfile(agbfile):
        print ('Calibration file not found. Check path.')
        exit(1)

    # calibrate agb file
    agb_dir = os.path.join(outpath, 'calibration')
    txtfile = os.path.join(agb_dir, 'calibration.txt') 

    # start timer
    t0 = time.clock()

    # if there is no calibration direcory, run calibration
    if not os.path.isdir(agb_dir):
        # Make dir, ignoring exception if (due to a race condition)
        # the dir just got made
        try:
            os.mkdir(agb_dir)
        except OSError as ose:
            import errno
            if ose.errno != errno.EEXIST:
                raise ose
            pass

    # open Silver-Behenate file
    agbimg = saxswaxs.load_image(agbfile, ext)
    hashkey = hashlib.sha256(agbimg).hexdigest()

    # check if we already calibrated this file, else do the 
    # calibration (it can be slow)
    #calibrationf = os.path.join(os.environ['HOME'], hashkey)
    calibrationf = os.path.join(os.environ['SCRATCH'], hashkey)
    if os.path.isfile(calibrationf):
        sys.stderr.write('loading calibration data from file.\n')
        a = saxswaxs.load_calibration(calibrationf)
    else:
        sys.stderr.write('Running the calibration routines.\n')
        a = calibrate_AgBeh(agbfile, ext)
        a.hash_ = hashkey
        try: saxswaxs.dump_calibration(calibrationf, a)
        except: sys.stderr.write('Can\'t write to scratch directory.\n')
    agb_time = time.clock() - t0
 
    # do this only once
    if start == 0:
        agbdir = os.path.join(outpath, 'calibration')
        if not os.path.isdir(agbdir):
            os.mkdir(agbdir)

        # remesh mask
        mask = saxswaxs.remesh_mask (agbfile, a) 
        saxswaxs.save_data (mask, agbfile, a, agbdir, "mask")

        # write calibration data to text file
        filename = os.path.join(agbdir, "calibration_data.txt")
        fp = open(filename, 'w')
        fp.write ('Direct Beam X = %f\n' % a.center_[0])
        fp.write ('Direct Beam Y = %f\n' % a.center_[1])
        fp.write ('Sample-Det Distance = %f\n' % a.sdd_)
        fp.write ('Q-Parallel min = %f\n' % a.qrange_[0])
        fp.write ('Q-Parallel max = %f\n' % a.qrange_[1])
        fp.write ('Q-Vertical min = %f\n' % a.qrange_[2])
        fp.write ('Q-Vertical max = %f\n' % a.qrange_[3])
        fp.close ()

    # collect edf files
    try:
        token = inpath+'/*.'+ext
        edfiles = glob.glob (token)
    except:
        edfiles = []
        for f in os.listdir(inpath):
            if f.endswith(ext):
                edfiles.append(os.path.join(inpath, f))

    # make sure we have data
    if len(edfiles) == 0:
        print "No edf file found"
        exit(1)

    # if index of last file is too large make it small
    if end is not None and end > len(edfiles):
        end = len(edfiles)

    # calibrate data
    t0 = time.clock()
    calibrate_data (edfiles[start:end], a, ext, outpath)
    t1 = time.clock() - t0
    n2 = end - start + 1
    t2 = t1 / n2
    print "AgB time: %f" % agb_time
    print "Remesh time:  total %f, images %d, time per image: %f" % (t1, n2, t2)

if __name__ == '__main__':
    main()
