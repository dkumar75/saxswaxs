# README #

calibrate data divides image calibration into two steps

	1. Calibrate AgB image, which is required for other images
	2. Calibrate data using AgB calibration data. The second part
		can be done is parallel


### What is this repository for? ###
The toolbox aims to automatically calibrate data produced at X-Ray Scattering beamlines.

* SAXS-WAXS data calibration toolbox
* Version 6.0

### How do I get set up? ###
* Dependencies
	* Numpy
	* Scipy
	* matplotlib
	* Fabio
	* PyFAI - for dpdak module
    * c++ compiler

* How to run tests
    run "test_calibrate_data.sh" to check installtion integrity

* Deployment instructions
    python setup.py build
    python setup.py install --install-base=<prefix>

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact